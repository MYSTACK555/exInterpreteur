﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter1
{
    class Program
    {
        //public string output { get; set; }
        static void Main(string[] args)
        {
            Console.WriteLine("écrivez le nom du Produit et la quantité ex: Lapin_de_pâque 4");
            while (true)
            {
                string expression = Console.ReadLine();//"Kinder 4";
                Parser p = new Parser(expression);
                Console.WriteLine(p.evaluate());
            }
        }
        public static int prodChercher { get; set; }
        public static string[,,] prod = new string[4, 2, 4] { { { "Lapin_de_pâque", "Cacao", "Lait","sirop" },   {"4" ,"100","25","2"} },
                                                            { { "Kinder",       "Cacao","Coconut","" },    {"2" ,"75" ,"5" ,"0"} },
                                                            { { "Choco_saveur", "Cacao","Rhum",""},        {"30","90" ,"15","0"} },
                                                            { { "Magic_choc",   "Cacao","Mana",""},        {"1","200" ,"74","0"} }
                                                       };
    }

    class Parser
    {
        public List<IExpression> lst = new List<IExpression>();
        
        public Parser(string s)
        {
            string[] str = s.Split(' ');
            int i = 0;
           
            while (i < str.Length)
            {
                if (i==0)
                    lst.Add(new TermExpress_Prod(str[0]));
                else
                    lst.Add(new TermExpress_Number(int.Parse(str[1])));
                i++;
            }
        }
        
        public string evaluate()
        {
            string context = "";
            foreach (IExpression e in lst) e.interpret(ref context);
            return context;
        }
    }

    class TermExpress_Number : IExpression
    {
        

        public void interpret(ref string p)
        {
            if (Program.prodChercher >= 0)
            {
                int i = 1;
                double vglobQteR = int.Parse(Program.prod[Program.prodChercher, 1, 0]),//item par recette,
                       vglobQte;

                while (i < Program.prod.GetLength(2))
                {
                    vglobQte = int.Parse(Program.prod[Program.prodChercher, 1, i]);//qte pour une recette
                    if (Program.prod[Program.prodChercher, 0, i] != "")
                        p += "\r\n" + Program.prod[Program.prodChercher, 0, i] + ": " + ((this.Qte / vglobQteR) * vglobQte).ToString();
                    i++;
                }
            }
        }
        private int Qte;

        public TermExpress_Number(int Qte)
        {
            this.Qte = Qte;
        }
    }

    class TermExpress_Prod : IExpression
    {
        private string prod;

        public TermExpress_Prod(string prod)
        {
            this.prod = prod;
        }

        public void interpret(ref string p)
        {
            int ind = 0;
            bool test = false;
            while (ind < Program.prod.GetLength(0) && !test)
            {
                if (this.prod == Program.prod[ind, 0, 0])
                    test = true;
                ind++;
            }

            if (!test)
            {
                Console.WriteLine("produit invalide");
                Program.prodChercher =-1;
                return;
            }
            else
                Program.prodChercher = ind - 1;
            p = this.prod.Replace('_', ' ');
        }
    }

    interface IExpression
    {
        void interpret(ref string p);
    }
}
